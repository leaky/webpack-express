module.exports = function (ngModule) {

    require('./header-styles.scss');
    require('./header-controller.js')(ngModule);

    ngModule.directive('appHeader', function () {
        return {
            restrict: 'E',
            scope: {},
            template: require('./header-template.html'),
            controllerAs: 'header',
            controller: 'appHeaderCtrl'
        };
    });

};
