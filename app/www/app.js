var angular = require('angular');
var ngModule = angular.module('app', []);

require('./components')(ngModule);
