(function () {

    'use strict';

    module.exports = function (grunt) {

        require('load-grunt-tasks')(grunt);

        grunt.initConfig({

            watch: {
                sass: {
                    files: [
                        './app/www/**/*.scss',
                    ],
                    options: {
                        livereload: true
                    }
                },
                scripts: {
                    files: [
                        './app/www/**/*.js'
                    ],
                    options: {
                        livereload: true
                    }
                },
                templates: {
                    files: [
                        './app/www/**/*.{html,ejs}'
                    ],
                    options: {
                        livereload: true
                    }
                }
            },

            nodemon: {
                dev: {
                    script: './app.js',
                    options: {
                        env: {
                            PORT: 3000
                        },
                        callback: function (nodemon) {
                            nodemon.on('restart', function (event) {
                                setTimeout(function () {
                                    require('fs').writeFileSync('.rebooted', 'rebooted');
                                }, 1000);
                            });
                        }
                    }
                }
            },

            concurrent: {
                dev: {
                    tasks: ['nodemon', 'watch'],
                    options: {
                        logConcurrentOutput: true
                    }
                }
            }

        });

        grunt.registerTask('dev', ['concurrent']);

    };

})();
