var express = require('express');
var path = require('path');
var httpProxy = require('http-proxy');
var http = require('http');
var app = express();



var isProduction = process.env.NODE_ENV === 'production';
var port = isProduction ? process.env.PORT : 3000;



var proxy = httpProxy.createProxyServer({
    changeOrigin: true,
    ws: true
});


app.set('view engine', 'ejs');
app.set('views', path.join('./app/www/views'));
app.use(express.static(path.join('./app/www')));

app.get('/', function (req, res) {
    res.render('index');
});

if (!isProduction) {

    var server = http.createServer(app);
    var bundler = require('./server/bundler.js');

    bundler();

    app.all('/assets/*', function (req, res) {
        console.log('foo');
        proxy.web(req, res, {
          target: 'http://127.0.0.1:3001'
        });
    });

    proxy.on('error', function (err) {
        console.log(err);
    });

    server.on('upgrade', function (req, socket, head) {
        proxy.ws(req, socket, head);
    });

    server.listen(port, function () {
        console.log('Server running on port ' + port);
    });

} else {

    app.listen(port, function () {
        console.log('Server running on port ' + port);
    });

}
