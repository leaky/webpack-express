var extractText = require('extract-text-webpack-plugin');
var webpack = require('webpack');
var path = require('path');

var config = {

    context: __dirname,
    devtool: 'eval-source-map',

    entry: [
        './app/www/app.js'
    ],

    output: {
        path: path.join('/app/www/assets'),
        filename: 'app.js'
    },

    module: {
        loaders: [
            { test: /\.html$/, loader: 'raw', exclude: /node_modules/ },
            { test: /\.js$/, loader: 'babel', exclude: /node_modules/ },
            { test: /\.scss$/, loader: extractText.extract('style', 'css!sass'), exclude: /node_modules/ }
        ]
    },

    plugins: [
        new extractText('styles.css')
    ]

};

// if (process.env.NODE_ENV === 'dev') {
//     config.plugins.push(new webpack.HotModuleReplacementPlugin());
// }

module.exports = config;
